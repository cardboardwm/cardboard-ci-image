FROM archlinux:latest

RUN pacman -Syu --noconfirm meson ninja wlroots mesa pkgconf wayland-protocols git gcc clang
